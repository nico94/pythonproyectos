import RPi.GPIO as GPIO
from gpiozero import LED
import time, sys
import urllib2
import pyrebase

FLOW_SENSOR = 17

#led = LED(17)

GPIO.setmode(GPIO.BCM)
GPIO.setup(FLOW_SENSOR, GPIO.IN, pull_up_down = GPIO.PUD_UP)

GPIO.setup(24, GPIO.OUT)

config = {
  "apiKey": "insert your key",
  "authDomain": "insert your domain",
  "databaseURL": "insert your url",
  "storageBucket": "insert your url"
}
firebase = pyrebase.initialize_app(config)  

global count
global mininterval
global starttime
global flow

global runtim
global litre

global db


count = 0
starttime = 0
flow = 0
litre = 0

def wait_internet():
    while True:
        try:
            res = urllib2.urlopen('http://google.com',timeout=1)
            return
        except urllib2.URLError:
            pass

         
def stream_handler(post):
    print(post["data"])
    global litre
    global db

    dat = db.child("smartank").child("chitkara").child("litre").get()
    print (dat.val())
    litre = dat.val()
    if(float(litre) != 0.0):
        print "MOtor on"
        GPIO.output(17, GPIO.LOW)


    
    
        
    
def main():
    global db
    global litre
    db = firebase.database()
    test = db.child("smartank").child("chitkara").stream(stream_handler)
    
        



def countPulse(channel):
   global count
   global starttime
   global flow
   global litre
   count = count+1
  
   currentTime = int(time.time())

   
   cflow = float(count) / 190
   count=0
   flow = flow + cflow
   #print count, litre
   print round(flow, 2)
   print float(litre)
   
   if(flow > float(litre)):
        print "motor off"
        GPIO.output(17, GPIO.HIGH)
        count=0
        flow = 0
        
       
      


GPIO.add_event_detect(FLOW_SENSOR, GPIO.FALLING, callback=countPulse)

main()


while True:
    try:

        
        time.sleep(1)
        
    except KeyboardInterrupt:
        print '\ncaught keyboard interrupt!, bye'
        GPIO.cleanup()
        sys.exit()
