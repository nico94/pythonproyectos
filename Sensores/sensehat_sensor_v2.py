#!/usr/bin/python
# Author: Ettore Gallina




from sense_hat import SenseHat

sense = SenseHat()

t = sense.get_temperature()
p = sense.get_pressure()
h = sense.get_humidity()

orientation = sense.get_orientation()
pitch = orientation['pitch']
roll = orientation['roll']
yaw = orientation['yaw']

msg = 'temp={0} press={1} hum={2} pitch={3} roll={4} yaw={5} '.format(t,p,h,pitch,roll,yaw)
print(msg)

