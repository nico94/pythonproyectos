#!/usr/bin/python
# Author: Ettore Gallina

import sys
from time import sleep
from sense_hat import SenseHat

sense = SenseHat()

command_type = text = sys.argv[1]

if command_type == 'show_message':
    text = sys.argv[2]
    r = int(sys.argv[3])
    g = int(sys.argv[4])
    b = int(sys.argv[5])
    rot = int(sys.argv[6])
    sense.set_rotation(rot)
    if r==0 and g==0 and b==0:
        sense.show_message(text, text_colour=[r,g,b], back_colour=[255,255,255])
        sleep(0.2)
        sense.clear()
    else:
        sense.show_message(text, text_colour=[r,g,b])
elif command_type == 'clear':
    r = int(sys.argv[2])
    g = int(sys.argv[3])
    b = int(sys.argv[4])
    sense.clear(r,g,b)

