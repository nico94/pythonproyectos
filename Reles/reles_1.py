#!/usr/bin/python
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# inicializar lista con pines
pinList = [17, 27, 22, 23, 18, 24, 25, 12]

# setear modo a los pines
for i in pinList:
    GPIO.setup(i, GPIO.OUT)
    GPIO.output(i, GPIO.HIGH)

# Tiempo para operaciones de dormir en loop
SleepTimeS = 0.1
SleepTimeL = 0.2

# loop
try:
    while True:
        for i in pinList:
            GPIO.output(i, GPIO.LOW)
            time.sleep(SleepTimeS)
            GPIO.output(i, GPIO.HIGH)
            time.sleep(SleepTimeS);
            GPIO.output(i, GPIO.LOW)
            time.sleep(SleepTimeS);
            GPIO.output(i, GPIO.HIGH)
            time.sleep(SleepTimeL);

# Terminar programa con teclado
except KeyboardInterrupt:
    print "  Salir"

    # Resetear los GPIO
    GPIO.cleanup()
