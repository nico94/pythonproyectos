import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# pines
pinList = [17, 27, 22, 23, 18, 24, 25, 12]

for i in pinList: 
    GPIO.setup(i, GPIO.OUT) 
    GPIO.output(i, GPIO.HIGH)

SleepTimeL = 0.2

try:
  while True:   
    for i in pinList:
        GPIO.output(i, GPIO.HIGH)
        time.sleep(SleepTimeL);
        GPIO.output(i, GPIO.LOW)
    pinList.reverse()

    for i in pinList:
        GPIO.output(i, GPIO.HIGH)
        time.sleep(SleepTimeL);
        GPIO.output(i, GPIO.LOW)
    pinList.reverse()

except KeyboardInterrupt:
  print "  Salir"

  GPIO.cleanup()