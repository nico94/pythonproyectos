import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# pines
pinList = [17, 27, 22, 23, 18, 24, 25, 12]

for i in pinList: 
    GPIO.setup(i, GPIO.OUT) 
    GPIO.output(i, GPIO.HIGH)

SleepTimeL = 1

try:
  GPIO.output(17, GPIO.LOW)
  print "UNO"
  time.sleep(SleepTimeL); 
  GPIO.output(27, GPIO.LOW)
  print "DOS"
  time.sleep(SleepTimeL);  
  GPIO.output(22, GPIO.LOW)
  print "TRES"
  time.sleep(SleepTimeL);
  GPIO.output(23, GPIO.LOW)
  print "CUATRO"
  time.sleep(SleepTimeL);
  GPIO.output(18, GPIO.LOW)
  print "CINCO"
  time.sleep(SleepTimeL);
  GPIO.output(24, GPIO.LOW)
  print "SEIS"
  time.sleep(SleepTimeL);
  GPIO.output(25, GPIO.LOW)
  print "SIETE"
  time.sleep(SleepTimeL);
  GPIO.output(12, GPIO.LOW)
  print "OCHO"
  time.sleep(SleepTimeL);
  GPIO.cleanup()
  print "Adios!"

except KeyboardInterrupt:
  print "  Salir"

  GPIO.cleanup()