import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# pines
pinList = [17, 27, 22, 23, 18, 24, 25, 12]

for i in pinList: 
    GPIO.setup(i, GPIO.OUT) 
    GPIO.output(i, GPIO.HIGH)

SleepTimeL = 0.1
try:
   count = 9
   while (count > 0):
      print '   Contador:', count
      for i in pinList:
         GPIO.output(i, GPIO.LOW)
         time.sleep(SleepTimeL);

      pinList.reverse()

      for i in pinList:
         GPIO.output(i, GPIO.HIGH)
         time.sleep(SleepTimeL);

      pinList.reverse()
      count = count - 1

except KeyboardInterrupt:
  print "  Salir"

  GPIO.cleanup()